# Baremetal STM32 Weather Station

This project is a fully baremetal weather detection system built using the STM32F446RE microcontroller. The primary objective is to interface with a DHT11 sensor to measure temperature and humidity and display the readings on a 1602 LCD screen. The project is implemented by directly accessing the microcontroller registers without using any abstraction layers like the HAL library. The system also includes an external interrupt feature triggered by a push button to display humidity data temporarily.

## Features

- **Clock Initialization**: Configured for 50 MHz by directly setting the appropriate registers.
- **Delay Functions**: Custom delay functions calibrated to the 50 MHz clock.
- **DHT11 Sensor Interface**: Interfacing with the DHT11 sensor for temperature and humidity data acquisition.
- **LCD Display**: Utilizes I2C communication to display data on a 1602 LCD screen using the PCF8574 IC.
- **External Interrupt**: Configured to trigger on a button press, displaying humidity data for 5 seconds.
- **Optimized Memory Management**: Enhanced memory management through custom linker script modifications for better performance and memory efficiency.

## Hardware Requirements

- STM32F446RE microcontroller
- DHT11 Temperature and Humidity Sensor
- 1602 LCD Display with I2C interface (PCF8574)
- Push Button
- Connecting wires and breadboard

## Software Requirements

- STM32CubeIDE
- ARM GCC Compiler

## Project Structure

- `RccConfig50MHz.c`: Configures the system clock to 50 MHz.
- `Delay.c`: Implements custom delay functions.
- `i2c.c`: I2C driver for the STM32F446RE.
- `i2c_lcd_pcf8574.c`: Functions to interface with the 1602 LCD via I2C.
- `main.c`: Main application code including sensor reading, LCD display logic, and external interrupt handling.
- `STM32F446RETX_FLASH.ld`: Custom linker script for optimized memory management.

## Getting Started

### Setup and Configuration

1. **Clone the repository**:
    ```sh
    git clone https://gitlab.com/hazim1998/baremetal-stm32-weather-station.git
    cd baremetal-stm32-weather-station
    ```

2. **Open the project in STM32CubeIDE**:
    - Open STM32CubeIDE and import the project from the cloned repository.

3. **Build and Flash**:
    - Build the project and flash it to your STM32F446RE board.

4. **Hardware Connections**:
    - Connect the DHT11 sensor to the appropriate GPIO pins.
    - Connect the 1602 LCD to the I2C pins.
    - Connect the push button to a GPIO pin configured for external interrupt.

### Pin Configuration

- **DHT11**:
    - Data Pin: PA1
- **LCD**:
    - SDA: PB9
    - SCL: PB8
- **Push Button**:
    - PA4 (configured for external interrupt)

### Running the Project

1. **Power the Board**:
    - Ensure your STM32F446RE board is powered via USB or external power source.
2. **Observe the LCD**:
    - The temperature reading should be displayed continuously.
3. **Trigger the Interrupt**:
    - Press the push button to display the humidity reading for 5 seconds.

## Project Details

### Clock Configuration

The system clock is configured to 50 MHz by modifying the RCC registers directly. This ensures the microcontroller operates at the desired speed for accurate timing and sensor communication.

### DHT11 Sensor Interface

The DHT11 sensor is interfaced by sending a start signal and reading the sensor's response. The data is acquired bit-by-bit, checking the data line's status at precise intervals.

### LCD Display

The 1602 LCD is connected via I2C using the PCF8574 IC. Custom functions are written to initialize the LCD, set cursor positions, and print strings and characters.

### External Interrupt

An external interrupt is configured on PA4. When the button is pressed, the interrupt handler sets a flag, and the main loop checks this flag to display the humidity reading.

### Linker Script Optimization

The project's memory management has been improved by modifying the default linker script. The updated linker script includes better memory region definitions and more efficient section allocations, leveraging the STM32F446RE's CCMRAM for specific data to optimize performance and memory usage.

## Future Improvements

1. **Add More Sensors**: Integrate additional sensors (e.g., barometric pressure sensor) to enhance the weather station capabilities.
2. **Data Logging**: Implement data logging functionality to store sensor readings over time, possibly using an SD card.
3. **Wireless Communication**: Add wireless modules (e.g., ESP8266) to send data to a remote server or cloud platform.
4. **Power Management**: Optimize power consumption by implementing low-power modes and efficient power management strategies.
5. **Graphical Display**: Upgrade to a graphical LCD or OLED display for more detailed and visually appealing data representation.

## Contribution Guidelines

Contributions are welcome! If you find a bug or want to add a feature, feel free to create a pull request.

## License

This project is licensed under the MIT License.

---

For any questions or suggestions, feel free to contact me at [hazim1998.p1@gmail.com].

