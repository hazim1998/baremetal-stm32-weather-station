#include "stm32f446xx.h"
#include "stm32f4xx.h"
#include "i2c.h"
#include "i2c_lcd_pcf8574.h"

#include "RccConfig.h"
#include "Delay.h"
#define GPIOA_BASE_ADDRESS ((uint32_t)0x40020000)

#define DHT11_PORT ((GPIO_TypeDef *) GPIOA_BASE_ADDRESS)
#define GPIO_PIN_1  ((uint16_t)0x0002)


uint8_t rh_byte1, rh_byte2, temp_byte1, temp_byte2;
uint16_t sum, rh,temp;
float temperature=0;
float humidity=0;
uint8_t correct_response =0;


void DHT11_Config(void){
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOHEN;
	GPIOA->OSPEEDR &= ~GPIO_OSPEEDR_OSPEED1;

	GPIOA->MODER &= ~(3<<8);  // Bits (3:2) = 0:0  --> PA4 in Input Mode

	GPIOA->PUPDR |=  (1<<8);  // Bits (3:2) = 1:0  --> PA4 is in Pull Down mode



}
void Interrupt_Config (void)
{
	/*************>>>>>>> STEPS FOLLOWED <<<<<<<<************

	1. Enable the SYSCNFG bit in RCC register
	2. Configure the EXTI configuration Regiter in the SYSCNFG
	3. Enable the EXTI using Interrupt Mask Register (IMR)
	4. Configure the Rising Edge / Falling Edge Trigger
	5. Set the Interrupt Priority
	6. Enable the interrupt

	********************************************************/

	RCC->APB2ENR |= (1<<14);  // Enable SYSCNFG

	SYSCFG->EXTICR[1] &= ~(0xf<<4);  // Bits[7:6:5:4] = (0:0:0:0)  -> configure EXTI1 line for PA4

	EXTI->IMR |= (1<<4);  // Bit[1] = 1  --> Disable the Mask on EXTI 4

	EXTI->FTSR &= ~(1<<4);  // Disable Rising Edge Trigger for PA4

	EXTI->RTSR |= (1<<4);  // Enable Falling Edge Trigger for PA4

	NVIC_SetPriority (EXTI4_IRQn, 1);  // Set Priority

	NVIC_EnableIRQ (EXTI4_IRQn);  // Enable Interrupt

}
int count = 0;
volatile int flag = 0;

void EXTI4_IRQHandler(void)
{


	if (EXTI->PR & (1<<4))    // If the PA4 triggered the interrupt
	{
		flag = 1;
		EXTI->PR |= (1<<4);  // Clear the interrupt flag by writing a 1
	}
}


void Host_Start(){
	DHT11_PORT->MODER |= GPIO_MODER_MODE1_0;//setting PA1 to output mode
	DHT11_PORT->BSRR |= GPIO_BSRR_BR1; //sending low start signal by pulling down voltage
	Delay_ms(18);
	DHT11_PORT->BSRR |= GPIO_BSRR_BS1;//pulling voltage up
	Delay_us(20);
	DHT11_PORT->MODER &= ~GPIO_MODER_MODE1;//setting PA1 to input mode and waiting for dht response
}
uint8_t Sensor_Response(){
		uint8_t Response = 0;
		uint8_t bitstatus;
		Delay_us(40);
		while ((DHT11_PORT->IDR & GPIO_PIN_1) != 0);//check after 40us if dht has responded with low signal
		if ((DHT11_PORT->IDR & GPIO_PIN_1) != 0) bitstatus = 1;//if dht doesn't respond
		else  bitstatus = 0;//if dht responds with lowsignal
		if (!bitstatus)
		{
			Delay_us(80);//check after 80us if dht has pulled up voltage
			if ((DHT11_PORT->IDR & GPIO_PIN_1) != 0) Response = 1;//dht has pulled up voltage
			else Response = -1;
		}
		while ((DHT11_PORT->IDR & GPIO_PIN_1) != 0);   // waiting for the pin (dht) to go low

		return Response;
}
uint8_t DHT11_ReadIT (void)
{
	uint8_t data = 0;
	    for (uint8_t i = 0; i < 8; i++) {//checking for 8 incoming bits
	        while (!(DHT11_PORT->IDR & GPIO_PIN_1));//waiting for voltage to be pulled high
	        Delay_us(35);
	        if (!(DHT11_PORT->IDR & GPIO_PIN_1)) {//check after 35 us if data line is now pulled low
	            data &= ~(1 << (7 - i));//if data line is pulled low, it means (0) bit is transmitted
	        } else {
	            data |= (1 << (7 - i));//if data line is still high, (1) bit is transmitted
	        }
	        while (DHT11_PORT->IDR & GPIO_PIN_1);//waiting for voltage to be pulled low for next bit
	    }
	    return data;//return 8 bit data
}
void Display_Temp(float Temp)
{
    char str[20] = {0};
    lcd_setcursor(0, 0);
    sprintf(str, "TEMP:- %.2f ", Temp);

    // Display the temperature string
    LCD_string(str);

    // Display the 'C' character
    LCD_data('C');
}

void Display_Rh(float Rh)
{
    char str[20] = {0};
    lcd_setcursor(0, 1); // Set cursor position to the second line, first column

    sprintf(str, "RH:- %.2f ", Rh);

    // Display the relative humidity string
    LCD_string(str);

    // Display the '%' character
    LCD_data('%');
}

int main(void){
	SysClockConfig();
	TIM6Config();

	DHT11_Config();
	Interrupt_Config();

	I2C_Config();
	LCD_init();
	lcd_clear();
	lcd_printLine1("Weather Data:"); // Display "DO IT>>>>" on the first line of the LCD
	Delay_ms(2000); // Wait for 2000 milliseconds (2 seconds)
	lcd_clear(); // Clear the LCD

	while(1){

		Display_Temp(temperature);


		Host_Start();
		correct_response= Sensor_Response();
		rh_byte1 = DHT11_ReadIT ();//8bit integral RH data
		rh_byte2 = DHT11_ReadIT ();//8bit decimal RH data
		temp_byte1 = DHT11_ReadIT ();//8bit integral temp data
		temp_byte2 = DHT11_ReadIT ();//8bit decimal temp data
		sum = DHT11_ReadIT();//8bit checksum

		temp = temp_byte1;
		rh = rh_byte1;
		temperature = (float) temp;
		humidity = (float) rh;
		if (flag) //when external interrupt is trigered ..
		{
		   Delay_ms (1);
			 Display_Rh(humidity);//display humididty
			 count++;
			 Delay_ms (5000);//for 5 sec
			 lcd_clear();
		   flag = 0;
		}

		Delay_ms(1000);


		}



}
