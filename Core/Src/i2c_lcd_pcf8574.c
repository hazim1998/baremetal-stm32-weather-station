#include "i2c_lcd_pcf8574.h"
#include "i2c.h"

uint8_t LCD_En = 0x04; // 00000100
uint8_t LCD_Rw = 0x00; // 00000000
uint8_t LCD_Rs = 0x01; // 00000001

void lcd_IO(unsigned char Data)
{
    I2C_Start();
    I2C_Address(pcf8574_WADDR);
    I2C_Write(Data | LCD_BACKLIGHT);
    I2C_Stop();
}

void pulse_enable(uint8_t data)
{
    lcd_IO(data | LCD_En);
    Delay_us(1);
    lcd_IO(data & ~LCD_En);
    Delay_us(50);
}

void lcd_write4bit(unsigned char nibble)
{
    nibble |= (LCD_Rs | LCD_Rw);
    lcd_IO(nibble | 0x04);
    lcd_IO(nibble & 0xFB);
    Delay_ms(display_rate);
}

void LCD_command(unsigned char command)
{
    LCD_Rs = 0b00000000;
    LCD_Rw = 0b00000000;
    uint8_t highnib = command & 0xf0;
    uint8_t lownib = (command << 4) & 0xf0;
    lcd_write4bit(highnib);
    lcd_write4bit(lownib);
}

void lcd_setcursor(uint8_t col, uint8_t row)
{
    static uint8_t offsets[] = {0x00, 0x40, 0x14, 0x54 };
    LCD_command((0x80 | (row << 6)) + col);
    Delay_us(40);
}

void lcd_clear()
{
    LCD_command(lcd_clear_all);
    Delay_ms(20);
}

void lcd_Rethome()
{
    LCD_command(lcd_rhome);
    Delay_ms(2);
}

void LCD_data(unsigned char data)
{
    LCD_Rs = 0b00000001;
    LCD_Rw = 0b00000000;
    uint8_t highnib = data & 0xf0;
    uint8_t lownib = (data << 4) & 0xf0;
    lcd_write4bit(highnib);
    lcd_write4bit(lownib);
}

void lcd_printint(int value)
{
    unsigned char thousands, hundreds, tens, ones;

    thousands = value / 1000;
    if (thousands != 0)
        LCD_data(thousands + 0x30);

    hundreds = ((value - thousands * 1000)) / 100;
    LCD_data(hundreds + 0x30);

    tens = (value % 100) / 10;
    LCD_data(tens + 0x30);

    ones = value % 10;
    LCD_data(ones + 0x30);
}

void lcd_printint_num(int value)
{
    unsigned char thousands, hundreds, tens, ones;

    thousands = value / 1000;
    if (thousands != 0)
        LCD_data(thousands + 0x30);

    hundreds = ((value - thousands * 1000) - 1) / 100;
    if (hundreds != 0)
        LCD_data(hundreds + 0x30);

    tens = (value % 100) / 10;
    if (tens != 0)
        LCD_data(tens + 0x30);

    ones = value % 10;
    LCD_data(ones + 0x30);
}

void LCD_string(const char *str)
{
    int i;
    for (i = 0; str[i] != 0; i++)
    {
        LCD_data(str[i]);
        Delay_us(45);
    }
}

void lcd_printLine1(const char *buff)
{
    lcd_setcursor(0, 0);
    LCD_string(buff);
}

void lcd_printLine2(const char *buff)
{
    lcd_setcursor(0, 1);
    LCD_string(buff);
}

void lcd_bit(unsigned char val)
{
    int ptr;
    for (ptr = 7; ptr >= 0; ptr--)
    {
        if ((val & (1 << ptr)) == 0)
        {
            LCD_string("0");
        }
        else
        {
            LCD_string("1");
        }
    }
}

void setramaddr(uint8_t address)
{
    LCD_command(address);
    Delay_us(50);
}

void LCD_sendpattern(uint8_t loc, uint8_t pattern[])
{
    int i;
    if (loc < 8)
    {
        setramaddr((lcd_SETCGRAMADDR + (loc * 8)));
        for (i = 0; i < 8; i++)
            LCD_data(pattern[i]);
    }
}

void LCD_printpattern(uint8_t loc)
{
    LCD_data((0x00 + loc));
}

void LCD_init() {
    I2C_Config(); // Initialize I2C
    Delay_us(100);
    lcd_IO(0x00);
    Delay_ms(25); // LCD Power ON delay always >15ms
    {
        LCD_command(lcd_4bit_2line); // Function set --> 8-bit mode is selected, 2 lines
        Delay_ms(5);
        LCD_command(lcd_4bit_2line); // Function set --> 8-bit mode is selected, 2 lines
        Delay_us(160);
        LCD_command(lcd_4bit_2line); // Function set --> 8-bit mode is selected, 2 lines
    }
    LCD_command(lcd_4bit_2line); // Function set --> 8-bit mode is selected, 2 lines
    Delay_us(45);
    LCD_command(lcd_DN_CF_BF); // Display ON/OFF control --> display is on, cursor is off, cursor blink is off
    Delay_ms(1);
    LCD_command(lcd_CML_DMF); // Entry mode set --> cursor moves to right, DRAM is incremented by 1, shift of display is off
    Delay_us(40);
}
