#ifndef I2C_LCD_PCF8574_H
#define I2C_LCD_PCF8574_H

#include <stdint.h>

// LCD Commands
#define lcd_clear_all 0x01
#define lcd_rhome 0x02
#define lcd_4bit_2line 0x28
#define lcd_DN_CF_BF 0x0C
#define lcd_CML_DMF 0x06
#define lcd_CMF_DMF 0x04
#define lcd_SETCGRAMADDR 0x40

#define pcf8574_WADDR 0x4E
#define LCD_BACKLIGHT 0x08
#define display_rate 50

// Function prototypes
void lcd_IO(unsigned char Data);
void pulse_enable(uint8_t data);
void lcd_write4bit(unsigned char nibble);
void LCD_command(unsigned char command);
void lcd_setcursor(uint8_t col, uint8_t row);
void lcd_clear();
void lcd_Rethome();
void LCD_data(unsigned char data);
void lcd_printint(int value);
void lcd_printint_num(int value);
void LCD_string(const char *str);
void lcd_printLine1(const char *buff);
void lcd_printLine2(const char *buff);
void lcd_bit(unsigned char val);
void setramaddr(uint8_t address);
void LCD_sendpattern(uint8_t loc, uint8_t pattern[]);
void LCD_printpattern(uint8_t loc);
void LCD_init();

#endif /* I2C_LCD_PCF8574_H */
