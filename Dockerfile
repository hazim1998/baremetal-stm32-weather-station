# Use an official Ubuntu as a base image
FROM ubuntu:20.04

# Set environment variables to avoid prompts during package installation
ENV DEBIAN_FRONTEND=noninteractive

# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    gcc-arm-none-eabi \
    gdb \
    make \
    cmake \
    git \
    wget \
    unzip \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    || { echo "Failed to install packages"; exit 1; }

# Set up workspace directory
WORKDIR /workspace

# Set the default command
CMD ["/bin/bash"]
